#!/bin/sh
set -e
# Strip only the top domain to get the zone id
DOMAIN=$(expr match "$CERTBOT_DOMAIN" '.*\.\(.*\..*\)')

# Get the vscale domain id
DOMAIN_ID=$(curl -s -X GET "https://api.vscale.io/v1/domains/" \
     -H "X-Token: $API_KEY" \
     -H "Content-Type: application/json" | \
     python -c "import sys,json;print(next(x['id'] for x in json.load(sys.stdin) if x['name'] == '$DOMAIN'))")

# Create TXT record
TXT_RECORD="_acme-challenge.$CERTBOT_DOMAIN"
RECORD_ID=$(curl -s -X POST "https://api.vscale.io/v1/domains/$DOMAIN_ID/records/" \
     -H "X-Token: $API_KEY" \
     -H "Content-Type: application/json" \
     -d '{"type":"TXT", "name":"'"$TXT_RECORD"'", "content":"'"$CERTBOT_VALIDATION"'", "ttl":120}' | \
     python -c "import sys,json;print(json.load(sys.stdin)['id'])")

# Save info for cleanup
if [[ ! -d /tmp/CERTBOT_$CERTBOT_DOMAIN ]]; then
    mkdir -m 0700 /tmp/CERTBOT_$CERTBOT_DOMAIN
fi

echo $DOMAIN_ID > /tmp/CERTBOT_$CERTBOT_DOMAIN/DOMAIN_ID
echo $RECORD_ID > /tmp/CERTBOT_$CERTBOT_DOMAIN/RECORD_ID

# Sleep to make sure the change has time to propagate over to DNS
sleep 10