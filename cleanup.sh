#!/bin/sh
set -e

if [ -f /tmp/CERTBOT_$CERTBOT_DOMAIN/DOMAIN_ID ]; then
        DOMAIN_ID=$(cat /tmp/CERTBOT_$CERTBOT_DOMAIN/DOMAIN_ID)
        rm -f /tmp/CERTBOT_$CERTBOT_DOMAIN/DOMAIN_ID
fi

if [ -f /tmp/CERTBOT_$CERTBOT_DOMAIN/RECORD_ID ]; then
        RECORD_ID=$(cat /tmp/CERTBOT_$CERTBOT_DOMAIN/RECORD_ID)
        rm -f /tmp/CERTBOT_$CERTBOT_DOMAIN/RECORD_ID
fi

# Remove the challenge TXT record from the domain
if [ -n "$DOMAIN_ID" ]; then
    if [ -n "$RECORD_ID" ]; then
        curl -s -X DELETE "https://api.vscale.io/v1/domains/$DOMAIN_ID/records/$RECORD_ID" \
            -H "X-Token: $API_KEY" \
            -H "Content-Type: application/json"
    fi
fi