FROM certbot/certbot:latest

RUN apk add --no-cache curl

COPY authenticator.sh /opt/certbot-vscale-dns/authenticator.sh
COPY cleanup.sh /opt/certbot-vscale-dns/cleanup.sh

ENTRYPOINT ["certbot", "--manual-auth-hook", \
    "/opt/certbot-vscale-dns/authenticator.sh", "--manual-cleanup-hook", "/opt/certbot-vscale-dns/cleanup.sh"]